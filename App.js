import React, { Component } from 'react'
import {
  ActivityIndicator,
  AppRegistry,
  StyleSheet,
  Text,
  View,     
} from 'react-native'

export default class App extends Component {
  render() {
    return (
      <View style={[styles.container, styles.horizontal]}>
          <ActivityIndicator size="large" color="pink" /> 
          <ActivityIndicator size="small" color="#00ff00" />
          <ActivityIndicator size="large" color="#6f6fd1" />
          <ActivityIndicator size="small" color="#8a9a8a" />
        </View>
    )   
  }
}

const styles = StyleSheet.create({
    
  container: {
    flex: 1,
    justifyContent: 'center'
   
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  }
})

AppRegistry.registerComponent('App', () => App)